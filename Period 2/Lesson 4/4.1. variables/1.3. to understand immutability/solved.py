my_integer = 10
my_float = 102.65
my_complex = 12j
my_string_1 = 'birinci sətir'
my_string_2 = "ikinci sətir"
my_string_3 = '''üçüncü sətir'''
my_string_4 = """dördüncü sətir"""
my_tuple = (1, 2, 205, 16.436, "salam dünya!")

# dəyişənlərin adreslərini id() funksiyasının köməkliyi ilə çap edirik
print("address of my_integer variable is:", id(my_integer))
print("address of my_float variable is:", id(my_float))
print("address of my_complex variable is:", id(my_complex))
print("address of my_string_1 variable is:", id(my_string_1))
print("address of my_string_2 variable is:", id(my_string_2))
print("address of my_string_3 variable is:", id(my_string_3))
print("address of my_string_4 variable is:", id(my_string_4))
print("address of my_tuple variable is:", id(my_tuple))

# dəyişənin dəyərini dəyişərək onun adresinin də dəyişdiyini yoxlayırıq
print("old address of my_integer variable was:", id(my_integer))
my_integer = 20
print("new address of my_integer variable is:", id(my_integer))

# dəyişənə fərqli tipdə dəyər mənimsətdikdə onun tipinin avtomatik dəyişdiyini yoxlayırıq
print("old type of my_integer variable was:", type(my_integer))
my_integer = "salam dunya!"
print("new type of my_integer variable is:", type(my_integer))