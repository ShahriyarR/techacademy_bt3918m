# Dəyişən yaradın və ona hal-hazırda olduğunuz cari folderin path-ini (adresini) mənimsədin

# Yaratdığınız dəyişəni echo vasitəsi ilə ekranda göstərin

# istifadəçinin home folderinə keçid edin

# keçid etdiyiniz direktoriyanın dəqiq path-ini echo vasitəsi ilə print edin

# home folderində test_folder yaradın

# yeni yaratdığınız test_folder-ə keçid edin (nisbi path-dən istifadə edərək)

# içərisində olduğunuz cari folder echo vasitəsi ilə print edin

# test_folder direktoriyasının içərisində sub_folder adlı alt direktoriya yaradın və bu direktoriyaya keçid edin

# içərisində olduğunuz cari folder echo vasitəsi ilə print edin

# yeni yaratdığınız sub_folderin içərisində əvvəli tmp_file_ ilə başlayan sonu sıralı rəqəmlə və .txt extension ilə bitən 100 ədəd fayl yaradın

# sonu 2 ilə bitən bütün faylları silin (məsələn: tmp_file_2.txt, tmp_file_12.txt və s.)

# nisbi path-dən istifadə edərək home folderə qayıdın

# test_folderin adını dəyişərək test_directory edin

# test_folderi içərisindəki fayl və folderlərlə birlikdə silin
