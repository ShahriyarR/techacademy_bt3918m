# parametrdən gələn rəqəmlərin ən böyüyünü tapan funksiya yazın
# =============================================================
def max_(*argc):
    myset = [i for i in argc if isinstance(i, int) or isinstance(i, float)]
    myset.sort(reverse=True)
    return myset[0]

print(max_(1,5,6,39,6,7))
# =============================================================

# parametrdən gələn bütün rəqəmlərin cəmini toplayan funksiya yazın
# =============================================================
def sum_(*argc):
    summary = 0
    for i in argc:
        if not isinstance(i, int) and not isinstance(i, float):
            continue
            
        summary += i
    return summary

print(sum_(8, 2, 3, 0, 7))
# =============================================================

# parametrdən gələn bütün rəqəmlərin hasilini qaytaran funksiya yazın
# =============================================================
def mul_(*argc):
    mul = 0
    for i in argc:
        if not isinstance(i, int) and not isinstance(i, float):
            continue
            
        mul *= i
    return mul

print(sum_(8, 2, 3, 50, 7))
# =============================================================

# Parametrdə gələn sətrə böyük və kiçik hərflərin sayını tapan funksiya yazın
# =============================================================
def case_calculator(s):
    upper = lower = 0
    for c in s:
        if c.isupper():
           upper += 1
        elif c.islower():
           lower += 1
        else:
           pass
    return (upper, lower)

case_calculator('TechAcademy is Coding Bootcamp')
# =============================================================

# parametrdə list qəbul edərək həmən listi unique edərək return edən funksiya yazın
# =============================================================
def unique_list(l):
    x = []
    for a in l:
        if a not in x:
            x.append(a)
    return x

print(unique_list([1,2,3,3,3,3,4,5]))
# =============================================================

# parametrdə ötürülən rəqəmin prime rəqəm olduğunu tapın.
# Qeyd: Prime rəqəmlər yalnızca özünə və 1-ə bölünür
# =============================================================
def is_prime(n):
    if n==1:
        return False
    elif n==2:
        return True;
    else:
        for x in range(2,n):
            if not n % x:
                return False
        return True 

print(is_prime(9))
# =============================================================

# parametrə ötürülmüş rəqəmlərdən yalnızca cüt olanları qaytarın
# =============================================================
def is_even(*argc):
    mylist = []
    for i in argc:
        if (isinstance(i, int) or isinstance(i, float)) and not i % 2:
            mylist.append(i)
    return mylist
print(is_even(1, 2, 3, 4, 5, 6, 7, 8, 9))
# =============================================================

# parametrdə ötürülmüş rəqəmin perfect olub olmamasını tapın.
# Qeyd: perfect rəqəmlər özünün bütün bölünənlərinin cəminə bərabərdir
# =============================================================
def is_perfect(n):
    sum = 0
    for x in range(1, n):
        if not n % x:
            sum += x
    return sum == n
print(perfect_number(6))
# =============================================================

# parametrdə ötürülmüş sətrin palindrom olub olmamasını yoxlayan funksiya yazın
# Qeyd: palindrom sətrlər tərsinə oxunduqda da eyni səslənənlərdir
# =============================================================
def is_palindrome(string):
    return string == string[::-1]
print(is_palindrome('azza'))
# =============================================================

# parametrdə ötürülmüş sətrin pangram olub olmamasını yoxlayan funksiya yazın
# Qeyd: pangram sözlər və ya cümlələr əlifbadakı bütün hərfləri özündə ən az bir dəfə olanlara deyilir
# =============================================================
def is_pangram(mystring):
    import string
    alphaset = set(string.ascii_lowercase)
    return alphaset <= set(mystring.lower())
 
print (is_pangram('The quick brown fox jumps over the lazy dog'))
# =============================================================

# parametrdə tire ilə ayrılmış sözlərdən ibarət string göndərilir. Bu sözləri sıralayıb (sort) daha sonra yenidən tire ilə bir-birindən ayıraraq qaytaran funksiya yazın
# =============================================================
def my_sorter(mystring):
    mylist = mystring.split("-")
    mylist.sort()
    return "-".join(mylist)

my_sorter("tech-academy-is-coding-bootcamp")
# =============================================================