# Avtomobil elanları
Avtomobil elanları proqramı terminalda işləyən sadə proqramdır. İstifadəçiyə sistemə avtomobil daxil etməyi və ya sistemdə olan maşınlardan birini almağa imkan verir

## Prerequisite
* inquirer və terminaltables kitabxanalarını araşdırın və pip vasitəsi ilə install edin. Proyektdə onlardan da istifadə etmək lazımdır.
* avtomobil məlumatlarını özündə saxlayan namedtuple yaradın
	* Maşının markası
	* Maşının rəngi
	* Maşının ili
	* Maşının mühərriki
	* Maşının getdiyi yol (km ilə)
	* Maşının qiyməti

## Qaydalar

Elə bir proqram yazın ki, o aşağıdakı funksionallıqları yerinə yetirə bilsin

- İstifadəçidən 10 ədəd müxtəlif maşınlar daxil etməyi tələb etsin
- Daha sonra maşını default olaraq ilinə görə sort edərək səliqəli cədvəldə bütün məlumatlarını göstərsin və növbəti addımda istifadəçinin nə etmək istədiyini soruşsun. Seçimlər aşağıdakılardır:
	- Cədvəli başqa sütuna görə sıralayın. Əgər bu seçim edildisə onda yeni seçim çıxır və bu seçimə əsasən cədvəl yenidən sort edilib səliqəli print edilir:
		- Markaya görə
		- Rəngə görə
		- ilinə görə
		- Mühərrikin gücünə görə
		- Getdiyi məsafəyə görə
		- Qiymətə görə
	- Maşın almaq. Əgər bu seçim edilərsə onda istifadəçidən növbə ilə müəyyən məbləğ və marka daxil etməsini tələb edin. Əgər daxil etdiyi məbləğ maşının qiymətindən azdırsa onda istifadəçiyə xəbərdarlıq edin və yenidən məbləğ daxil etməsini tələb edin. Əgər eynidirsə onda maşını siyahıdan silin və təşəkkür edərək proqramı dayandırın. Əgər məbləğ maşının qiymətindən çoxdursa onda məbləğdən maşının qiymətini çıxın və istifadəçiyə bildirin ki, filan qədər qalıq qaytardınız. Daha sonra təşəkkür edərək proqramı dayandırın
	- Yeni maşın daxil etmək. Bu zaman yenidən istifadəçiyə maşın daxil etmək üçün imkan yaradın